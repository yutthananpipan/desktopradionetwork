﻿using DigitalSync.DB;
using SQLite;
using System;
using System.Collections.Generic;
using System.Data;

namespace DigitalSync.Models
{
    public class Profile
    {
        [PrimaryKey]
        public int Id { get; set; }
        public int Zone { get; set; }
        public int Channel { get; set; }
        public string Name { get; set; }
        public string Remark { get; set; }

        public Profile(int id, int zone, int channel, string name, string remark)
        {
            this.Id = id;
            this.Zone = zone;
            this.Channel = channel;
            this.Name = name;
            this.Remark = remark;
        }

        public Profile() {}

        public static List<Profile> GetProfiles()
        {
            List<Profile> profiles = new List<Profile>();
            try
            {
                var db = DBProvider.CreateConnection();
                var p = db.Table<Profile>().ToList();
                foreach (Profile profile in p)
                    profiles.Add(profile);

                db.Close();
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return profiles;
        }

        public static void Update(Profile profile)
        {
            var db = DBProvider.CreateConnection();
            var result = db.Update(profile);
            db.Close();
        }
    }
}
