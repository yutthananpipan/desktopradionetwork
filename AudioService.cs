﻿using RadioNetworkDesktop.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Media;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RadioNetworkDesktop
{
    class AudioService
    {
        private static int AUDIO_PORT = 65432;
        private string ADDRESS = "172.16.10.249";

        private EndPoint epFrom = new IPEndPoint(IPAddress.Any, AUDIO_PORT);
        private AsyncCallback recv = null;
        private UdpClient udpClient;

        Thread thread_connect;
        Thread thread_listening;

        public delegate void OnMessage(RadioData data);
        public event OnMessage MessageListening;

        public void StartAsync()
        {
            thread_connect = new Thread(ClientConnect);
            thread_listening = new Thread(ClientListening);


            thread_connect.IsBackground = true;
            thread_listening.IsBackground = true;
            thread_connect.Start();
            thread_listening.Start();

        }

        public void Distroy()
        {
            try
            {
                udpClient.Close();
                thread_connect.Abort();
                thread_listening.Abort();
            }catch(Exception e)
            {
                e.ToString();
            }
        }

        private void ClientConnect()
        {
            try
            {
                udpClient = new UdpClient();
                udpClient.Connect(ADDRESS, AUDIO_PORT);
                Send("Connect");
  
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        private void ClientListening()
        {
            try
            {
                UdpClient udpServer = new UdpClient(AUDIO_PORT);

                while (true)
                {
                    var remoteEP = new IPEndPoint(IPAddress.Any, AUDIO_PORT);
                    byte[] data = udpServer.Receive(ref remoteEP); // listen on port 11000
                    string returnData = Encoding.ASCII.GetString(data);

                    Console.Write("receive data from " + returnData.ToString());

                    try
                    {
                        
                    }
                    catch (Exception e)
                    {
                        e.ToString();
                    } 

                }
            }
            catch(Exception e)
            {
                e.ToString();
            }
        }

        public void Send(string text)
        {
            try
            {
                // Sends a message to the host to which you have connected.
                Byte[] sendBytes = Encoding.ASCII.GetBytes(text);
                udpClient.Send(sendBytes, sendBytes.Length);
            }
            catch (Exception e)
            {
                e.ToString();
            }
        }

    }
}
