﻿using DigitalSync.DB;
using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace DigitalSync.Models
{
    public class HistoryCall
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public int Zone { get; set; }
        public int Channel { get; set; }
        public string Uid { get; set; }
        public DateTime Datetime { get; set; }

        public HistoryCall() { }

        public HistoryCall(int zone, int channel, string uid, DateTime datetime)
        {
            this.Zone = zone;
            this.Channel = channel;
            this.Uid = uid;
            this.Datetime = datetime;
        }

        public static void Update(HistoryCall historyCall)
        {
            var db = DBProvider.CreateConnection();
            var result = db.Update(historyCall);
            db.Close();
        }

        public static List<HistoryCall> GetHistoryCall()
        {
            List<HistoryCall> historyCall = new List<HistoryCall>();
            try
            {
                var db = DBProvider.CreateConnection();
                string query = $"SELECT * FROM HistoryCall ORDER BY Id DESC LIMIT 30";
                var results = db.Query<HistoryCall>(query);
                foreach (HistoryCall h in results)
                    historyCall.Add(h);

                db.Close();
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return historyCall;
        }

        internal static void Insert(HistoryCall historyCall)
        {
            var db = DBProvider.CreateConnection();
            var result = db.Insert(historyCall);
            db.Close();
        }
    }
}
