﻿using DigitalSync.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DigitalSync
{
    public partial class ButtonFuncUserControl : UserControl
    {
        public String FunctionName;

        public Profile profile;
        PTTUserControl pttUserControl;

        public Label CH { get { return this.labelCH; } set { this.labelCH = value; } }
        public Label FUNCTION { get { return this.labelFunctionNumber; } set { this.labelFunctionNumber = value; } }
        public Label NAME { get { return this.labelName; } set { this.labelName = value; } }

        public delegate void OnSelectChange();
        public event OnSelectChange selectListening;

        public ButtonFuncUserControl(PTTUserControl pttUserControl, Profile profile)
        {
            this.pttUserControl = pttUserControl;
            this.profile = profile;
            InitializeComponent();
            InitTextDisplay();

        }

        public void InitTextDisplay()
        {
            SetSelect(Properties.Settings.Default.profile_select == profile.Id);
            this.labelName.Parent.Padding = new Padding((labelName.Parent.Width - labelName.Width) / 2, 0, 0, 0);
        }

        private void edit_Click(object sender, EventArgs e)
        {
            DialogResult result = ProfileUpdate.MSGShow(this);
            try
            {
                if (result == DialogResult.OK)
                {
                    CH.Text = "CH" + profile.Channel;
                    FUNCTION.Text = "F" + (profile.Id + 1).ToString();
                    NAME.Text = profile.Name;
                    InitTextDisplay();
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
        }

        private void ButtonFuncUserControl_Resize(object sender, EventArgs e)
        {
            this.Dock = DockStyle.Fill;
        }

        private void SetSelect(bool isSelect)
        {
            int chDisplay = Properties.Settings.Default.CHANNELID;
            this.Body.BackgroundImage = (isSelect && chDisplay == profile.Channel) ? Properties.Resources.border_function_blue
                : Properties.Resources.border_function_gray;
            this.FUNCTION.ForeColor = (isSelect && chDisplay == profile.Channel) ? Color.White : Color.FromArgb(80, 80, 80);
            this.CH.ForeColor = (isSelect && chDisplay == profile.Channel) ? Color.White : Color.FromArgb(80, 80, 80);
            this.NAME.ForeColor = (isSelect && chDisplay == profile.Channel) ? Color.White : Color.FromArgb(80, 80, 80);
        }

        private void button_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.profile_select = profile.Id;
            Properties.Settings.Default.Save();
            selectListening();
        }
    }
}
