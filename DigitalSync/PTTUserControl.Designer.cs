﻿
namespace DigitalSync
{
    partial class PTTUserControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label_SocketStatus = new System.Windows.Forms.Label();
            this.pictureBox_SocketStatus = new System.Windows.Forms.PictureBox();
            this.labelUUID = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel9 = new System.Windows.Forms.Panel();
            this.panelDisplay = new System.Windows.Forms.Panel();
            this.panel13 = new System.Windows.Forms.Panel();
            this.panel18 = new System.Windows.Forms.Panel();
            this.panel22 = new System.Windows.Forms.Panel();
            this.labelPTTStatus_Display = new System.Windows.Forms.Label();
            this.panel21 = new System.Windows.Forms.Panel();
            this.labelTime_Display = new System.Windows.Forms.Label();
            this.panel20 = new System.Windows.Forms.Panel();
            this.labelName_Display = new System.Windows.Forms.Label();
            this.panel19 = new System.Windows.Forms.Panel();
            this.labelSendUID_Display = new System.Windows.Forms.Label();
            this.panel15 = new System.Windows.Forms.Panel();
            this.panel17 = new System.Windows.Forms.Panel();
            this.volumeLevel = new System.Windows.Forms.PictureBox();
            this.Speaker = new System.Windows.Forms.PictureBox();
            this.panel14 = new System.Windows.Forms.Panel();
            this.panel16 = new System.Windows.Forms.Panel();
            this.rsiLevel = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.panel6 = new System.Windows.Forms.Panel();
            this.labelCH_Display = new System.Windows.Forms.Label();
            this.panel8 = new System.Windows.Forms.Panel();
            this.panel10 = new System.Windows.Forms.Panel();
            this.panel_CH = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.panel12 = new System.Windows.Forms.Panel();
            this.channelDown = new System.Windows.Forms.PictureBox();
            this.panel11 = new System.Windows.Forms.Panel();
            this.channelUp = new System.Windows.Forms.PictureBox();
            this.panel7 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.EmergencyButton = new EnhancedGlassButton.GlassButton();
            this.PTTButton = new EnhancedGlassButton.GlassButton();
            this.panelFunction = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_SocketStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel3.SuspendLayout();
            this.panel9.SuspendLayout();
            this.panelDisplay.SuspendLayout();
            this.panel13.SuspendLayout();
            this.panel18.SuspendLayout();
            this.panel22.SuspendLayout();
            this.panel21.SuspendLayout();
            this.panel20.SuspendLayout();
            this.panel19.SuspendLayout();
            this.panel15.SuspendLayout();
            this.panel17.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.volumeLevel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Speaker)).BeginInit();
            this.panel14.SuspendLayout();
            this.panel16.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rsiLevel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.panel6.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel10.SuspendLayout();
            this.panel_CH.SuspendLayout();
            this.panel12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.channelDown)).BeginInit();
            this.panel11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.channelUp)).BeginInit();
            this.panel5.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.pictureBox_SocketStatus);
            this.panel1.Controls.Add(this.labelUUID);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(10, 10);
            this.panel1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(15, 0, 15, 0);
            this.panel1.Size = new System.Drawing.Size(894, 38);
            this.panel1.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label_SocketStatus);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel2.Location = new System.Drawing.Point(649, 0);
            this.panel2.Name = "panel2";
            this.panel2.Padding = new System.Windows.Forms.Padding(0, 7, 0, 7);
            this.panel2.Size = new System.Drawing.Size(200, 38);
            this.panel2.TabIndex = 11;
            // 
            // label_SocketStatus
            // 
            this.label_SocketStatus.AutoSize = true;
            this.label_SocketStatus.Dock = System.Windows.Forms.DockStyle.Right;
            this.label_SocketStatus.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label_SocketStatus.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(190)))), ((int)(((byte)(190)))), ((int)(((byte)(190)))));
            this.label_SocketStatus.Location = new System.Drawing.Point(87, 7);
            this.label_SocketStatus.Name = "label_SocketStatus";
            this.label_SocketStatus.Size = new System.Drawing.Size(113, 23);
            this.label_SocketStatus.TabIndex = 11;
            this.label_SocketStatus.Text = "Disconnected";
            // 
            // pictureBox_SocketStatus
            // 
            this.pictureBox_SocketStatus.BackgroundImage = global::DigitalSync.Properties.Resources.connect_machine;
            this.pictureBox_SocketStatus.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox_SocketStatus.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox_SocketStatus.Location = new System.Drawing.Point(849, 0);
            this.pictureBox_SocketStatus.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pictureBox_SocketStatus.Name = "pictureBox_SocketStatus";
            this.pictureBox_SocketStatus.Size = new System.Drawing.Size(30, 38);
            this.pictureBox_SocketStatus.TabIndex = 9;
            this.pictureBox_SocketStatus.TabStop = false;
            // 
            // labelUUID
            // 
            this.labelUUID.AutoSize = true;
            this.labelUUID.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelUUID.Font = new System.Drawing.Font("Segoe UI", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.labelUUID.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(196)))), ((int)(((byte)(196)))));
            this.labelUUID.Location = new System.Drawing.Point(45, 0);
            this.labelUUID.Name = "labelUUID";
            this.labelUUID.Size = new System.Drawing.Size(119, 35);
            this.labelUUID.TabIndex = 8;
            this.labelUUID.Text = "Unknown";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = global::DigitalSync.Properties.Resources.profile_default;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox1.Location = new System.Drawing.Point(15, 0);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(30, 38);
            this.pictureBox1.TabIndex = 7;
            this.pictureBox1.TabStop = false;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.panel9);
            this.panel3.Controls.Add(this.panel8);
            this.panel3.Controls.Add(this.panel7);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(10, 48);
            this.panel3.Name = "panel3";
            this.panel3.Padding = new System.Windows.Forms.Padding(5);
            this.panel3.Size = new System.Drawing.Size(894, 311);
            this.panel3.TabIndex = 1;
            // 
            // panel9
            // 
            this.panel9.Controls.Add(this.panelDisplay);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel9.Location = new System.Drawing.Point(193, 5);
            this.panel9.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel9.Name = "panel9";
            this.panel9.Padding = new System.Windows.Forms.Padding(0, 0, 0, 10);
            this.panel9.Size = new System.Drawing.Size(508, 301);
            this.panel9.TabIndex = 3;
            // 
            // panelDisplay
            // 
            this.panelDisplay.BackgroundImage = global::DigitalSync.Properties.Resources.border;
            this.panelDisplay.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panelDisplay.Controls.Add(this.panel13);
            this.panelDisplay.Controls.Add(this.panel6);
            this.panelDisplay.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelDisplay.Location = new System.Drawing.Point(0, 0);
            this.panelDisplay.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panelDisplay.Name = "panelDisplay";
            this.panelDisplay.Padding = new System.Windows.Forms.Padding(40, 40, 83, 35);
            this.panelDisplay.Size = new System.Drawing.Size(508, 291);
            this.panelDisplay.TabIndex = 2;
            // 
            // panel13
            // 
            this.panel13.BackColor = System.Drawing.Color.Transparent;
            this.panel13.Controls.Add(this.panel18);
            this.panel13.Controls.Add(this.panel15);
            this.panel13.Controls.Add(this.panel14);
            this.panel13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel13.Location = new System.Drawing.Point(40, 88);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(385, 168);
            this.panel13.TabIndex = 1;
            // 
            // panel18
            // 
            this.panel18.Controls.Add(this.panel22);
            this.panel18.Controls.Add(this.panel21);
            this.panel18.Controls.Add(this.panel20);
            this.panel18.Controls.Add(this.panel19);
            this.panel18.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel18.Location = new System.Drawing.Point(60, 0);
            this.panel18.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel18.Name = "panel18";
            this.panel18.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.panel18.Size = new System.Drawing.Size(265, 168);
            this.panel18.TabIndex = 4;
            // 
            // panel22
            // 
            this.panel22.BackColor = System.Drawing.Color.Transparent;
            this.panel22.Controls.Add(this.labelPTTStatus_Display);
            this.panel22.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel22.Location = new System.Drawing.Point(10, 114);
            this.panel22.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel22.Name = "panel22";
            this.panel22.Size = new System.Drawing.Size(245, 39);
            this.panel22.TabIndex = 3;
            // 
            // labelPTTStatus_Display
            // 
            this.labelPTTStatus_Display.AutoSize = true;
            this.labelPTTStatus_Display.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelPTTStatus_Display.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.labelPTTStatus_Display.ForeColor = System.Drawing.Color.Gray;
            this.labelPTTStatus_Display.Location = new System.Drawing.Point(0, 0);
            this.labelPTTStatus_Display.Name = "labelPTTStatus_Display";
            this.labelPTTStatus_Display.Size = new System.Drawing.Size(159, 32);
            this.labelPTTStatus_Display.TabIndex = 9;
            this.labelPTTStatus_Display.Text = "Disconnected";
            // 
            // panel21
            // 
            this.panel21.BackColor = System.Drawing.Color.Transparent;
            this.panel21.Controls.Add(this.labelTime_Display);
            this.panel21.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel21.Location = new System.Drawing.Point(10, 86);
            this.panel21.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel21.Name = "panel21";
            this.panel21.Size = new System.Drawing.Size(245, 28);
            this.panel21.TabIndex = 2;
            // 
            // labelTime_Display
            // 
            this.labelTime_Display.AutoSize = true;
            this.labelTime_Display.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelTime_Display.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.labelTime_Display.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(196)))), ((int)(((byte)(196)))));
            this.labelTime_Display.Location = new System.Drawing.Point(0, 0);
            this.labelTime_Display.Name = "labelTime_Display";
            this.labelTime_Display.Size = new System.Drawing.Size(94, 28);
            this.labelTime_Display.TabIndex = 16;
            this.labelTime_Display.Text = "12:00 PM";
            // 
            // panel20
            // 
            this.panel20.BackColor = System.Drawing.Color.Transparent;
            this.panel20.Controls.Add(this.labelName_Display);
            this.panel20.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel20.Location = new System.Drawing.Point(10, 34);
            this.panel20.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel20.Name = "panel20";
            this.panel20.Size = new System.Drawing.Size(245, 52);
            this.panel20.TabIndex = 1;
            // 
            // labelName_Display
            // 
            this.labelName_Display.AutoSize = true;
            this.labelName_Display.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelName_Display.Font = new System.Drawing.Font("Segoe UI", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.labelName_Display.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(196)))), ((int)(((byte)(196)))));
            this.labelName_Display.Location = new System.Drawing.Point(0, 0);
            this.labelName_Display.Name = "labelName_Display";
            this.labelName_Display.Size = new System.Drawing.Size(122, 46);
            this.labelName_Display.TabIndex = 16;
            this.labelName_Display.Text = "NAME";
            // 
            // panel19
            // 
            this.panel19.BackColor = System.Drawing.Color.Transparent;
            this.panel19.Controls.Add(this.labelSendUID_Display);
            this.panel19.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel19.Location = new System.Drawing.Point(10, 0);
            this.panel19.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel19.Name = "panel19";
            this.panel19.Size = new System.Drawing.Size(245, 34);
            this.panel19.TabIndex = 0;
            // 
            // labelSendUID_Display
            // 
            this.labelSendUID_Display.AutoSize = true;
            this.labelSendUID_Display.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelSendUID_Display.Font = new System.Drawing.Font("Segoe UI", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.labelSendUID_Display.ForeColor = System.Drawing.Color.DeepSkyBlue;
            this.labelSendUID_Display.Location = new System.Drawing.Point(0, 0);
            this.labelSendUID_Display.Name = "labelSendUID_Display";
            this.labelSendUID_Display.Size = new System.Drawing.Size(125, 35);
            this.labelSendUID_Display.TabIndex = 9;
            this.labelSendUID_Display.Text = "Broadcast";
            // 
            // panel15
            // 
            this.panel15.BackColor = System.Drawing.Color.Transparent;
            this.panel15.Controls.Add(this.panel17);
            this.panel15.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel15.Location = new System.Drawing.Point(325, 0);
            this.panel15.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(60, 168);
            this.panel15.TabIndex = 3;
            // 
            // panel17
            // 
            this.panel17.BackColor = System.Drawing.Color.Transparent;
            this.panel17.Controls.Add(this.volumeLevel);
            this.panel17.Controls.Add(this.Speaker);
            this.panel17.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel17.Location = new System.Drawing.Point(0, 0);
            this.panel17.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel17.Name = "panel17";
            this.panel17.Padding = new System.Windows.Forms.Padding(0, 0, 0, 16);
            this.panel17.Size = new System.Drawing.Size(50, 168);
            this.panel17.TabIndex = 4;
            // 
            // volumeLevel
            // 
            this.volumeLevel.BackgroundImage = global::DigitalSync.Properties.Resources.volume_level_10;
            this.volumeLevel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.volumeLevel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.volumeLevel.Location = new System.Drawing.Point(0, 0);
            this.volumeLevel.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.volumeLevel.Name = "volumeLevel";
            this.volumeLevel.Size = new System.Drawing.Size(50, 122);
            this.volumeLevel.TabIndex = 14;
            this.volumeLevel.TabStop = false;
            // 
            // Speaker
            // 
            this.Speaker.BackgroundImage = global::DigitalSync.Properties.Resources.speaker;
            this.Speaker.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.Speaker.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.Speaker.Location = new System.Drawing.Point(0, 122);
            this.Speaker.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Speaker.Name = "Speaker";
            this.Speaker.Size = new System.Drawing.Size(50, 30);
            this.Speaker.TabIndex = 13;
            this.Speaker.TabStop = false;
            this.Speaker.Click += new System.EventHandler(this.Speaker_Click);
            // 
            // panel14
            // 
            this.panel14.BackColor = System.Drawing.Color.Transparent;
            this.panel14.Controls.Add(this.panel16);
            this.panel14.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel14.Location = new System.Drawing.Point(0, 0);
            this.panel14.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(60, 168);
            this.panel14.TabIndex = 2;
            // 
            // panel16
            // 
            this.panel16.BackColor = System.Drawing.Color.Transparent;
            this.panel16.Controls.Add(this.rsiLevel);
            this.panel16.Controls.Add(this.pictureBox4);
            this.panel16.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel16.Location = new System.Drawing.Point(10, 0);
            this.panel16.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel16.Name = "panel16";
            this.panel16.Padding = new System.Windows.Forms.Padding(0, 0, 0, 12);
            this.panel16.Size = new System.Drawing.Size(50, 168);
            this.panel16.TabIndex = 4;
            // 
            // rsiLevel
            // 
            this.rsiLevel.BackgroundImage = global::DigitalSync.Properties.Resources.rsi_level;
            this.rsiLevel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.rsiLevel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rsiLevel.Location = new System.Drawing.Point(0, 0);
            this.rsiLevel.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.rsiLevel.Name = "rsiLevel";
            this.rsiLevel.Size = new System.Drawing.Size(50, 126);
            this.rsiLevel.TabIndex = 14;
            this.rsiLevel.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackgroundImage = global::DigitalSync.Properties.Resources.rsi;
            this.pictureBox4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox4.Location = new System.Drawing.Point(0, 126);
            this.pictureBox4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(50, 30);
            this.pictureBox4.TabIndex = 13;
            this.pictureBox4.TabStop = false;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.Transparent;
            this.panel6.Controls.Add(this.labelCH_Display);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel6.Location = new System.Drawing.Point(40, 40);
            this.panel6.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel6.Name = "panel6";
            this.panel6.Padding = new System.Windows.Forms.Padding(5, 5, 0, 0);
            this.panel6.Size = new System.Drawing.Size(385, 48);
            this.panel6.TabIndex = 0;
            // 
            // labelCH_Display
            // 
            this.labelCH_Display.AutoSize = true;
            this.labelCH_Display.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelCH_Display.Font = new System.Drawing.Font("Segoe UI", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.labelCH_Display.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(196)))), ((int)(((byte)(196)))));
            this.labelCH_Display.Location = new System.Drawing.Point(5, 5);
            this.labelCH_Display.Name = "labelCH_Display";
            this.labelCH_Display.Size = new System.Drawing.Size(71, 37);
            this.labelCH_Display.TabIndex = 15;
            this.labelCH_Display.Text = "CH1";
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.panel10);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel8.Location = new System.Drawing.Point(5, 5);
            this.panel8.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(188, 301);
            this.panel8.TabIndex = 2;
            // 
            // panel10
            // 
            this.panel10.Controls.Add(this.panel_CH);
            this.panel10.Controls.Add(this.panel12);
            this.panel10.Controls.Add(this.panel11);
            this.panel10.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel10.Location = new System.Drawing.Point(108, 0);
            this.panel10.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel10.Name = "panel10";
            this.panel10.Padding = new System.Windows.Forms.Padding(0, 50, 0, 50);
            this.panel10.Size = new System.Drawing.Size(80, 301);
            this.panel10.TabIndex = 1;
            // 
            // panel_CH
            // 
            this.panel_CH.Controls.Add(this.label1);
            this.panel_CH.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel_CH.Location = new System.Drawing.Point(0, 117);
            this.panel_CH.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel_CH.Name = "panel_CH";
            this.panel_CH.Size = new System.Drawing.Size(80, 67);
            this.panel_CH.TabIndex = 14;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(196)))), ((int)(((byte)(196)))));
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 37);
            this.label1.TabIndex = 14;
            this.label1.Text = "CH";
            // 
            // panel12
            // 
            this.panel12.Controls.Add(this.channelDown);
            this.panel12.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel12.Location = new System.Drawing.Point(0, 184);
            this.panel12.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(80, 67);
            this.panel12.TabIndex = 13;
            // 
            // channelDown
            // 
            this.channelDown.BackgroundImage = global::DigitalSync.Properties.Resources.arrow_down;
            this.channelDown.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.channelDown.Dock = System.Windows.Forms.DockStyle.Top;
            this.channelDown.Location = new System.Drawing.Point(0, 0);
            this.channelDown.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.channelDown.Name = "channelDown";
            this.channelDown.Size = new System.Drawing.Size(80, 50);
            this.channelDown.TabIndex = 12;
            this.channelDown.TabStop = false;
            this.channelDown.Click += new System.EventHandler(this.channelDown_Click);
            // 
            // panel11
            // 
            this.panel11.Controls.Add(this.channelUp);
            this.panel11.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel11.Location = new System.Drawing.Point(0, 50);
            this.panel11.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(80, 67);
            this.panel11.TabIndex = 12;
            // 
            // channelUp
            // 
            this.channelUp.BackgroundImage = global::DigitalSync.Properties.Resources.arrow_up;
            this.channelUp.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.channelUp.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.channelUp.Location = new System.Drawing.Point(0, 17);
            this.channelUp.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.channelUp.Name = "channelUp";
            this.channelUp.Size = new System.Drawing.Size(80, 50);
            this.channelUp.TabIndex = 13;
            this.channelUp.TabStop = false;
            this.channelUp.Click += new System.EventHandler(this.channelUp_Click);
            // 
            // panel7
            // 
            this.panel7.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel7.Location = new System.Drawing.Point(701, 5);
            this.panel7.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(188, 301);
            this.panel7.TabIndex = 1;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.panel5.Controls.Add(this.EmergencyButton);
            this.panel5.Controls.Add(this.PTTButton);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel5.Location = new System.Drawing.Point(10, 554);
            this.panel5.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel5.Name = "panel5";
            this.panel5.Padding = new System.Windows.Forms.Padding(20, 0, 20, 0);
            this.panel5.Size = new System.Drawing.Size(894, 80);
            this.panel5.TabIndex = 3;
            // 
            // EmergencyButton
            // 
            this.EmergencyButton.AlternativeFocusBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(249)))), ((int)(((byte)(151)))), ((int)(((byte)(0)))));
            this.EmergencyButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(249)))), ((int)(((byte)(151)))), ((int)(((byte)(0)))));
            this.EmergencyButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.EmergencyButton.Dock = System.Windows.Forms.DockStyle.Right;
            this.EmergencyButton.Font = new System.Drawing.Font("Segoe UI", 25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.EmergencyButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.EmergencyButton.GlowColor = System.Drawing.Color.FromArgb(((int)(((byte)(165)))), ((int)(((byte)(92)))), ((int)(((byte)(0)))));
            this.EmergencyButton.InnerBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(249)))), ((int)(((byte)(151)))), ((int)(((byte)(0)))));
            this.EmergencyButton.Location = new System.Drawing.Point(545, 0);
            this.EmergencyButton.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.EmergencyButton.Name = "EmergencyButton";
            this.EmergencyButton.OuterBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(249)))), ((int)(((byte)(151)))), ((int)(((byte)(0)))));
            this.EmergencyButton.ShineColor = System.Drawing.Color.FromArgb(((int)(((byte)(249)))), ((int)(((byte)(151)))), ((int)(((byte)(0)))));
            this.EmergencyButton.Size = new System.Drawing.Size(329, 80);
            this.EmergencyButton.TabIndex = 3;
            this.EmergencyButton.Text = "EMERGENCY";
            this.EmergencyButton.Click += new System.EventHandler(this.EmergencyButton_Click);
            this.EmergencyButton.KeyDown += new System.Windows.Forms.KeyEventHandler(this.PTTButton_KeyDown);
            this.EmergencyButton.KeyUp += new System.Windows.Forms.KeyEventHandler(this.PTTButton_KeyUp);
            // 
            // PTTButton
            // 
            this.PTTButton.AlternativeFocusBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(173)))), ((int)(((byte)(240)))));
            this.PTTButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(173)))), ((int)(((byte)(240)))));
            this.PTTButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.PTTButton.Dock = System.Windows.Forms.DockStyle.Left;
            this.PTTButton.Font = new System.Drawing.Font("Segoe UI", 25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.PTTButton.ForeColor = System.Drawing.Color.Lime;
            this.PTTButton.GlowColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(110)))), ((int)(((byte)(156)))));
            this.PTTButton.InnerBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(178)))), ((int)(((byte)(248)))));
            this.PTTButton.Location = new System.Drawing.Point(20, 0);
            this.PTTButton.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.PTTButton.Name = "PTTButton";
            this.PTTButton.OuterBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(178)))), ((int)(((byte)(248)))));
            this.PTTButton.ShineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(178)))), ((int)(((byte)(248)))));
            this.PTTButton.Size = new System.Drawing.Size(501, 80);
            this.PTTButton.TabIndex = 2;
            this.PTTButton.Text = "PTT";
            this.PTTButton.KeyDown += new System.Windows.Forms.KeyEventHandler(this.PTTButton_KeyDown);
            this.PTTButton.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.PTTButton_KeyPress);
            this.PTTButton.KeyUp += new System.Windows.Forms.KeyEventHandler(this.PTTButton_KeyUp);
            this.PTTButton.MouseDown += new System.Windows.Forms.MouseEventHandler(this.PTTButton_MouseDown);
            this.PTTButton.MouseUp += new System.Windows.Forms.MouseEventHandler(this.PTTButton_MouseUp);
            // 
            // panelFunction
            // 
            this.panelFunction.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.panelFunction.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelFunction.Location = new System.Drawing.Point(10, 359);
            this.panelFunction.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panelFunction.Name = "panelFunction";
            this.panelFunction.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.panelFunction.Size = new System.Drawing.Size(894, 195);
            this.panelFunction.TabIndex = 4;
            // 
            // PTTUserControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Controls.Add(this.panelFunction);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel1);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "PTTUserControl";
            this.Padding = new System.Windows.Forms.Padding(10);
            this.Size = new System.Drawing.Size(914, 644);
            this.Load += new System.EventHandler(this.PTTUserControl_Load);
            this.SizeChanged += new System.EventHandler(this.PTTUserControl_SizeChanged);
            this.Click += new System.EventHandler(this.PTTUserControl_Click);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.PTTButton_KeyDown);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.PTTButton_KeyUp);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_SocketStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel9.ResumeLayout(false);
            this.panelDisplay.ResumeLayout(false);
            this.panel13.ResumeLayout(false);
            this.panel18.ResumeLayout(false);
            this.panel22.ResumeLayout(false);
            this.panel22.PerformLayout();
            this.panel21.ResumeLayout(false);
            this.panel21.PerformLayout();
            this.panel20.ResumeLayout(false);
            this.panel20.PerformLayout();
            this.panel19.ResumeLayout(false);
            this.panel19.PerformLayout();
            this.panel15.ResumeLayout(false);
            this.panel17.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.volumeLevel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Speaker)).EndInit();
            this.panel14.ResumeLayout(false);
            this.panel16.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.rsiLevel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.panel10.ResumeLayout(false);
            this.panel_CH.ResumeLayout(false);
            this.panel_CH.PerformLayout();
            this.panel12.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.channelDown)).EndInit();
            this.panel11.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.channelUp)).EndInit();
            this.panel5.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label labelUUID;
        private System.Windows.Forms.PictureBox pictureBox_SocketStatus;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel5;
        private EnhancedGlassButton.GlassButton PTTButton;
        private EnhancedGlassButton.GlassButton EmergencyButton;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Panel panelDisplay;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Panel panel_CH;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.PictureBox channelDown;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.PictureBox channelUp;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label labelCH_Display;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label_SocketStatus;
        private System.Windows.Forms.Panel panel15;
        private System.Windows.Forms.Panel panel17;
        private System.Windows.Forms.PictureBox volumeLevel;
        private System.Windows.Forms.PictureBox Speaker;
        private System.Windows.Forms.Panel panel16;
        private System.Windows.Forms.PictureBox rsiLevel;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.Panel panel18;
        private System.Windows.Forms.Panel panel22;
        private System.Windows.Forms.Panel panel21;
        private System.Windows.Forms.Panel panel20;
        private System.Windows.Forms.Panel panel19;
        private System.Windows.Forms.Label labelSendUID_Display;
        private System.Windows.Forms.Label labelName_Display;
        private System.Windows.Forms.Label labelPTTStatus_Display;
        private System.Windows.Forms.Label labelTime_Display;
        private System.Windows.Forms.Panel panelFunction;
    }
}
