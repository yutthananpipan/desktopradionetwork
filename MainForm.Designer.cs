﻿
namespace RadioNetworkDesktop
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.top = new System.Windows.Forms.Panel();
            this.appLogo = new System.Windows.Forms.PictureBox();
            this.companyLogo = new System.Windows.Forms.PictureBox();
            this.bottom = new System.Windows.Forms.Panel();
            this.panel32 = new System.Windows.Forms.Panel();
            this.panel_DTMF = new System.Windows.Forms.Panel();
            this.label_DTMF = new System.Windows.Forms.Label();
            this.panel26 = new System.Windows.Forms.Panel();
            this.panel28 = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.panel27 = new System.Windows.Forms.Panel();
            this.panel29 = new System.Windows.Forms.Panel();
            this.panel31 = new System.Windows.Forms.Panel();
            this.label9 = new System.Windows.Forms.Label();
            this.panel30 = new System.Windows.Forms.Panel();
            this.label8 = new System.Windows.Forms.Label();
            this.panel22 = new System.Windows.Forms.Panel();
            this.panel23 = new System.Windows.Forms.Panel();
            this.panel24 = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.panel25 = new System.Windows.Forms.Panel();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.panel18 = new System.Windows.Forms.Panel();
            this.panel19 = new System.Windows.Forms.Panel();
            this.panel20 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.panel21 = new System.Windows.Forms.Panel();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel14 = new System.Windows.Forms.Panel();
            this.panel15 = new System.Windows.Forms.Panel();
            this.panel16 = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.panel17 = new System.Windows.Forms.Panel();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.panel10 = new System.Windows.Forms.Panel();
            this.panel11 = new System.Windows.Forms.Panel();
            this.panel12 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.panel13 = new System.Windows.Forms.Panel();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.panel8 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.panel9 = new System.Windows.Forms.Panel();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.panel0 = new System.Windows.Forms.FlowLayoutPanel();
            this.center = new System.Windows.Forms.Panel();
            this.center_left = new System.Windows.Forms.Panel();
            this.panel_gmap = new System.Windows.Forms.Panel();
            this.center_right = new System.Windows.Forms.FlowLayoutPanel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.top.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.appLogo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.companyLogo)).BeginInit();
            this.bottom.SuspendLayout();
            this.panel32.SuspendLayout();
            this.panel_DTMF.SuspendLayout();
            this.panel26.SuspendLayout();
            this.panel28.SuspendLayout();
            this.panel27.SuspendLayout();
            this.panel29.SuspendLayout();
            this.panel31.SuspendLayout();
            this.panel30.SuspendLayout();
            this.panel22.SuspendLayout();
            this.panel23.SuspendLayout();
            this.panel24.SuspendLayout();
            this.panel25.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            this.panel18.SuspendLayout();
            this.panel19.SuspendLayout();
            this.panel20.SuspendLayout();
            this.panel21.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel14.SuspendLayout();
            this.panel15.SuspendLayout();
            this.panel16.SuspendLayout();
            this.panel17.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.panel10.SuspendLayout();
            this.panel11.SuspendLayout();
            this.panel12.SuspendLayout();
            this.panel13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.center.SuspendLayout();
            this.center_left.SuspendLayout();
            this.SuspendLayout();
            // 
            // top
            // 
            this.top.BackColor = System.Drawing.Color.Transparent;
            this.top.Controls.Add(this.appLogo);
            this.top.Controls.Add(this.companyLogo);
            this.top.Dock = System.Windows.Forms.DockStyle.Top;
            this.top.Location = new System.Drawing.Point(10, 10);
            this.top.Name = "top";
            this.top.Size = new System.Drawing.Size(1562, 66);
            this.top.TabIndex = 0;
            // 
            // appLogo
            // 
            this.appLogo.BackgroundImage = global::RadioNetworkDesktop.Properties.Resources.Digital_Sync_logo;
            this.appLogo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.appLogo.Dock = System.Windows.Forms.DockStyle.Right;
            this.appLogo.Location = new System.Drawing.Point(1312, 0);
            this.appLogo.Name = "appLogo";
            this.appLogo.Size = new System.Drawing.Size(250, 66);
            this.appLogo.TabIndex = 1;
            this.appLogo.TabStop = false;
            // 
            // companyLogo
            // 
            this.companyLogo.BackgroundImage = global::RadioNetworkDesktop.Properties.Resources.drc_logo_white;
            this.companyLogo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.companyLogo.Dock = System.Windows.Forms.DockStyle.Left;
            this.companyLogo.Location = new System.Drawing.Point(0, 0);
            this.companyLogo.Name = "companyLogo";
            this.companyLogo.Size = new System.Drawing.Size(177, 66);
            this.companyLogo.TabIndex = 0;
            this.companyLogo.TabStop = false;
            // 
            // bottom
            // 
            this.bottom.BackColor = System.Drawing.Color.Transparent;
            this.bottom.Controls.Add(this.panel32);
            this.bottom.Controls.Add(this.panel26);
            this.bottom.Controls.Add(this.panel22);
            this.bottom.Controls.Add(this.panel18);
            this.bottom.Controls.Add(this.panel3);
            this.bottom.Controls.Add(this.panel14);
            this.bottom.Controls.Add(this.panel10);
            this.bottom.Controls.Add(this.panel1);
            this.bottom.Controls.Add(this.panel0);
            this.bottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.bottom.Location = new System.Drawing.Point(10, 720);
            this.bottom.Name = "bottom";
            this.bottom.Size = new System.Drawing.Size(1562, 123);
            this.bottom.TabIndex = 1;
            // 
            // panel32
            // 
            this.panel32.Controls.Add(this.panel_DTMF);
            this.panel32.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel32.Location = new System.Drawing.Point(577, 0);
            this.panel32.Name = "panel32";
            this.panel32.Padding = new System.Windows.Forms.Padding(20, 50, 20, 0);
            this.panel32.Size = new System.Drawing.Size(145, 123);
            this.panel32.TabIndex = 12;
            // 
            // panel_DTMF
            // 
            this.panel_DTMF.BackColor = System.Drawing.Color.Transparent;
            this.panel_DTMF.BackgroundImage = global::RadioNetworkDesktop.Properties.Resources.border_gray;
            this.panel_DTMF.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.panel_DTMF.Controls.Add(this.label_DTMF);
            this.panel_DTMF.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel_DTMF.Location = new System.Drawing.Point(20, 50);
            this.panel_DTMF.Name = "panel_DTMF";
            this.panel_DTMF.Padding = new System.Windows.Forms.Padding(20, 8, 0, 5);
            this.panel_DTMF.Size = new System.Drawing.Size(105, 46);
            this.panel_DTMF.TabIndex = 1;
            this.panel_DTMF.Click += new System.EventHandler(this.DTMF_Click);
            // 
            // label_DTMF
            // 
            this.label_DTMF.AutoSize = true;
            this.label_DTMF.BackColor = System.Drawing.Color.Transparent;
            this.label_DTMF.Dock = System.Windows.Forms.DockStyle.Top;
            this.label_DTMF.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold);
            this.label_DTMF.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.label_DTMF.Location = new System.Drawing.Point(20, 8);
            this.label_DTMF.Name = "label_DTMF";
            this.label_DTMF.Size = new System.Drawing.Size(67, 28);
            this.label_DTMF.TabIndex = 6;
            this.label_DTMF.Text = "DTMF";
            this.label_DTMF.Click += new System.EventHandler(this.DTMF_Click);
            // 
            // panel26
            // 
            this.panel26.BackColor = System.Drawing.Color.Transparent;
            this.panel26.Controls.Add(this.panel28);
            this.panel26.Controls.Add(this.panel27);
            this.panel26.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel26.Location = new System.Drawing.Point(0, 0);
            this.panel26.Name = "panel26";
            this.panel26.Size = new System.Drawing.Size(571, 123);
            this.panel26.TabIndex = 11;
            // 
            // panel28
            // 
            this.panel28.Controls.Add(this.label7);
            this.panel28.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel28.Location = new System.Drawing.Point(0, 83);
            this.panel28.Name = "panel28";
            this.panel28.Size = new System.Drawing.Size(571, 40);
            this.panel28.TabIndex = 1;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Dock = System.Windows.Forms.DockStyle.Top;
            this.label7.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(0, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(362, 28);
            this.label7.TabIndex = 6;
            this.label7.Text = "Digital Research and Consulting Co., Ltd.";
            // 
            // panel27
            // 
            this.panel27.Controls.Add(this.panel29);
            this.panel27.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel27.Location = new System.Drawing.Point(0, 0);
            this.panel27.Name = "panel27";
            this.panel27.Size = new System.Drawing.Size(571, 77);
            this.panel27.TabIndex = 0;
            // 
            // panel29
            // 
            this.panel29.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.panel29.Controls.Add(this.panel31);
            this.panel29.Controls.Add(this.panel30);
            this.panel29.Location = new System.Drawing.Point(2, 17);
            this.panel29.Name = "panel29";
            this.panel29.Padding = new System.Windows.Forms.Padding(5);
            this.panel29.Size = new System.Drawing.Size(462, 55);
            this.panel29.TabIndex = 0;
            // 
            // panel31
            // 
            this.panel31.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(38)))), ((int)(((byte)(37)))));
            this.panel31.Controls.Add(this.label9);
            this.panel31.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel31.Location = new System.Drawing.Point(242, 5);
            this.panel31.Name = "panel31";
            this.panel31.Size = new System.Drawing.Size(215, 45);
            this.panel31.TabIndex = 1;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Segoe UI", 15F);
            this.label9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(196)))), ((int)(((byte)(196)))));
            this.label9.Location = new System.Drawing.Point(34, 5);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(150, 35);
            this.label9.TabIndex = 7;
            this.label9.Text = "11 JAN 2021";
            // 
            // panel30
            // 
            this.panel30.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(38)))), ((int)(((byte)(37)))));
            this.panel30.Controls.Add(this.label8);
            this.panel30.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel30.Location = new System.Drawing.Point(5, 5);
            this.panel30.Name = "panel30";
            this.panel30.Size = new System.Drawing.Size(220, 45);
            this.panel30.TabIndex = 0;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Segoe UI", 15F);
            this.label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(196)))), ((int)(((byte)(196)))));
            this.label8.Location = new System.Drawing.Point(44, 4);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(146, 35);
            this.label8.TabIndex = 6;
            this.label8.Text = "12:56:24 PM";
            // 
            // panel22
            // 
            this.panel22.Controls.Add(this.panel23);
            this.panel22.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel22.Location = new System.Drawing.Point(722, 0);
            this.panel22.Name = "panel22";
            this.panel22.Padding = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.panel22.Size = new System.Drawing.Size(140, 123);
            this.panel22.TabIndex = 10;
            // 
            // panel23
            // 
            this.panel23.Controls.Add(this.panel24);
            this.panel23.Controls.Add(this.panel25);
            this.panel23.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel23.Location = new System.Drawing.Point(2, 0);
            this.panel23.Name = "panel23";
            this.panel23.Size = new System.Drawing.Size(136, 123);
            this.panel23.TabIndex = 5;
            // 
            // panel24
            // 
            this.panel24.Controls.Add(this.label6);
            this.panel24.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel24.Location = new System.Drawing.Point(0, 77);
            this.panel24.Name = "panel24";
            this.panel24.Padding = new System.Windows.Forms.Padding(30, 0, 0, 0);
            this.panel24.Size = new System.Drawing.Size(136, 38);
            this.panel24.TabIndex = 6;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Dock = System.Windows.Forms.DockStyle.Top;
            this.label6.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(196)))), ((int)(((byte)(196)))));
            this.label6.Location = new System.Drawing.Point(30, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(78, 20);
            this.label6.TabIndex = 5;
            this.label6.Text = "LOCATION";
            // 
            // panel25
            // 
            this.panel25.Controls.Add(this.pictureBox6);
            this.panel25.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel25.Location = new System.Drawing.Point(0, 0);
            this.panel25.Name = "panel25";
            this.panel25.Padding = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.panel25.Size = new System.Drawing.Size(136, 77);
            this.panel25.TabIndex = 1;
            // 
            // pictureBox6
            // 
            this.pictureBox6.BackgroundImage = global::RadioNetworkDesktop.Properties.Resources.location;
            this.pictureBox6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox6.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox6.Location = new System.Drawing.Point(0, 32);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(136, 40);
            this.pictureBox6.TabIndex = 3;
            this.pictureBox6.TabStop = false;
            // 
            // panel18
            // 
            this.panel18.Controls.Add(this.panel19);
            this.panel18.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel18.Location = new System.Drawing.Point(862, 0);
            this.panel18.Name = "panel18";
            this.panel18.Padding = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.panel18.Size = new System.Drawing.Size(140, 123);
            this.panel18.TabIndex = 9;
            // 
            // panel19
            // 
            this.panel19.Controls.Add(this.panel20);
            this.panel19.Controls.Add(this.panel21);
            this.panel19.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel19.Location = new System.Drawing.Point(2, 0);
            this.panel19.Name = "panel19";
            this.panel19.Size = new System.Drawing.Size(136, 123);
            this.panel19.TabIndex = 5;
            // 
            // panel20
            // 
            this.panel20.Controls.Add(this.label5);
            this.panel20.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel20.Location = new System.Drawing.Point(0, 77);
            this.panel20.Name = "panel20";
            this.panel20.Padding = new System.Windows.Forms.Padding(35, 0, 0, 0);
            this.panel20.Size = new System.Drawing.Size(136, 38);
            this.panel20.TabIndex = 6;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Dock = System.Windows.Forms.DockStyle.Top;
            this.label5.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(196)))), ((int)(((byte)(196)))));
            this.label5.Location = new System.Drawing.Point(35, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(67, 20);
            this.label5.TabIndex = 5;
            this.label5.Text = "HISTORY";
            // 
            // panel21
            // 
            this.panel21.Controls.Add(this.pictureBox5);
            this.panel21.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel21.Location = new System.Drawing.Point(0, 0);
            this.panel21.Name = "panel21";
            this.panel21.Padding = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.panel21.Size = new System.Drawing.Size(136, 77);
            this.panel21.TabIndex = 1;
            // 
            // pictureBox5
            // 
            this.pictureBox5.BackgroundImage = global::RadioNetworkDesktop.Properties.Resources.history;
            this.pictureBox5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox5.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox5.Location = new System.Drawing.Point(0, 32);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(136, 40);
            this.pictureBox5.TabIndex = 3;
            this.pictureBox5.TabStop = false;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.panel4);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel3.Location = new System.Drawing.Point(1002, 0);
            this.panel3.Name = "panel3";
            this.panel3.Padding = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.panel3.Size = new System.Drawing.Size(140, 123);
            this.panel3.TabIndex = 8;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.panel5);
            this.panel4.Controls.Add(this.panel6);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(2, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(136, 123);
            this.panel4.TabIndex = 5;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.label1);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.Location = new System.Drawing.Point(0, 77);
            this.panel5.Name = "panel5";
            this.panel5.Padding = new System.Windows.Forms.Padding(25, 0, 0, 0);
            this.panel5.Size = new System.Drawing.Size(136, 38);
            this.panel5.TabIndex = 6;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(196)))), ((int)(((byte)(196)))));
            this.label1.Location = new System.Drawing.Point(25, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(90, 20);
            this.label1.TabIndex = 5;
            this.label1.Text = "INDIVIDUAL";
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.pictureBox1);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel6.Location = new System.Drawing.Point(0, 0);
            this.panel6.Name = "panel6";
            this.panel6.Padding = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.panel6.Size = new System.Drawing.Size(136, 77);
            this.panel6.TabIndex = 1;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = global::RadioNetworkDesktop.Properties.Resources.individual;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox1.Location = new System.Drawing.Point(0, 32);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(136, 40);
            this.pictureBox1.TabIndex = 3;
            this.pictureBox1.TabStop = false;
            // 
            // panel14
            // 
            this.panel14.Controls.Add(this.panel15);
            this.panel14.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel14.Location = new System.Drawing.Point(1142, 0);
            this.panel14.Name = "panel14";
            this.panel14.Padding = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.panel14.Size = new System.Drawing.Size(140, 123);
            this.panel14.TabIndex = 7;
            // 
            // panel15
            // 
            this.panel15.Controls.Add(this.panel16);
            this.panel15.Controls.Add(this.panel17);
            this.panel15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel15.Location = new System.Drawing.Point(2, 0);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(136, 123);
            this.panel15.TabIndex = 5;
            // 
            // panel16
            // 
            this.panel16.Controls.Add(this.label4);
            this.panel16.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel16.Location = new System.Drawing.Point(0, 77);
            this.panel16.Name = "panel16";
            this.panel16.Padding = new System.Windows.Forms.Padding(40, 0, 0, 0);
            this.panel16.Size = new System.Drawing.Size(136, 38);
            this.panel16.TabIndex = 6;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Dock = System.Windows.Forms.DockStyle.Top;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(196)))), ((int)(((byte)(196)))));
            this.label4.Location = new System.Drawing.Point(40, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(57, 20);
            this.label4.TabIndex = 5;
            this.label4.Text = "GROUP";
            // 
            // panel17
            // 
            this.panel17.Controls.Add(this.pictureBox4);
            this.panel17.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel17.Location = new System.Drawing.Point(0, 0);
            this.panel17.Name = "panel17";
            this.panel17.Padding = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.panel17.Size = new System.Drawing.Size(136, 77);
            this.panel17.TabIndex = 1;
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackgroundImage = global::RadioNetworkDesktop.Properties.Resources.group;
            this.pictureBox4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox4.Location = new System.Drawing.Point(0, 32);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(136, 40);
            this.pictureBox4.TabIndex = 3;
            this.pictureBox4.TabStop = false;
            // 
            // panel10
            // 
            this.panel10.Controls.Add(this.panel11);
            this.panel10.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel10.Location = new System.Drawing.Point(1282, 0);
            this.panel10.Name = "panel10";
            this.panel10.Padding = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.panel10.Size = new System.Drawing.Size(140, 123);
            this.panel10.TabIndex = 6;
            // 
            // panel11
            // 
            this.panel11.Controls.Add(this.panel12);
            this.panel11.Controls.Add(this.panel13);
            this.panel11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel11.Location = new System.Drawing.Point(2, 0);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(136, 123);
            this.panel11.TabIndex = 5;
            // 
            // panel12
            // 
            this.panel12.Controls.Add(this.label3);
            this.panel12.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel12.Location = new System.Drawing.Point(0, 77);
            this.panel12.Name = "panel12";
            this.panel12.Padding = new System.Windows.Forms.Padding(37, 0, 0, 0);
            this.panel12.Size = new System.Drawing.Size(136, 38);
            this.panel12.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Top;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(196)))), ((int)(((byte)(196)))));
            this.label3.Location = new System.Drawing.Point(37, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(61, 20);
            this.label3.TabIndex = 5;
            this.label3.Text = "LAYOUT";
            // 
            // panel13
            // 
            this.panel13.Controls.Add(this.pictureBox3);
            this.panel13.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel13.Location = new System.Drawing.Point(0, 0);
            this.panel13.Name = "panel13";
            this.panel13.Padding = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.panel13.Size = new System.Drawing.Size(136, 77);
            this.panel13.TabIndex = 1;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackgroundImage = global::RadioNetworkDesktop.Properties.Resources.layout;
            this.pictureBox3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox3.Location = new System.Drawing.Point(0, 32);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(136, 40);
            this.pictureBox3.TabIndex = 3;
            this.pictureBox3.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.panel7);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel1.Location = new System.Drawing.Point(1422, 0);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.panel1.Size = new System.Drawing.Size(140, 123);
            this.panel1.TabIndex = 5;
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.panel8);
            this.panel7.Controls.Add(this.panel9);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel7.Location = new System.Drawing.Point(2, 0);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(136, 123);
            this.panel7.TabIndex = 5;
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.label2);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel8.Location = new System.Drawing.Point(0, 77);
            this.panel8.Name = "panel8";
            this.panel8.Padding = new System.Windows.Forms.Padding(35, 0, 0, 0);
            this.panel8.Size = new System.Drawing.Size(136, 38);
            this.panel8.TabIndex = 6;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Top;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(196)))), ((int)(((byte)(196)))));
            this.label2.Location = new System.Drawing.Point(35, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(66, 20);
            this.label2.TabIndex = 5;
            this.label2.Text = "SETTING";
            // 
            // panel9
            // 
            this.panel9.Controls.Add(this.pictureBox2);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel9.Location = new System.Drawing.Point(0, 0);
            this.panel9.Name = "panel9";
            this.panel9.Padding = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.panel9.Size = new System.Drawing.Size(136, 77);
            this.panel9.TabIndex = 1;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackgroundImage = global::RadioNetworkDesktop.Properties.Resources.setting;
            this.pictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox2.Location = new System.Drawing.Point(0, 32);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(136, 40);
            this.pictureBox2.TabIndex = 3;
            this.pictureBox2.TabStop = false;
            // 
            // panel0
            // 
            this.panel0.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel0.Location = new System.Drawing.Point(1562, 0);
            this.panel0.Name = "panel0";
            this.panel0.Padding = new System.Windows.Forms.Padding(20);
            this.panel0.Size = new System.Drawing.Size(0, 123);
            this.panel0.TabIndex = 2;
            // 
            // center
            // 
            this.center.BackColor = System.Drawing.Color.Transparent;
            this.center.Controls.Add(this.center_left);
            this.center.Controls.Add(this.center_right);
            this.center.Controls.Add(this.panel2);
            this.center.Dock = System.Windows.Forms.DockStyle.Fill;
            this.center.Location = new System.Drawing.Point(10, 76);
            this.center.Name = "center";
            this.center.Size = new System.Drawing.Size(1562, 644);
            this.center.TabIndex = 2;
            // 
            // center_left
            // 
            this.center_left.BackColor = System.Drawing.Color.Transparent;
            this.center_left.Controls.Add(this.panel_gmap);
            this.center_left.Dock = System.Windows.Forms.DockStyle.Fill;
            this.center_left.Location = new System.Drawing.Point(0, 0);
            this.center_left.Name = "center_left";
            this.center_left.Padding = new System.Windows.Forms.Padding(0, 0, 10, 0);
            this.center_left.Size = new System.Drawing.Size(648, 644);
            this.center_left.TabIndex = 6;
            // 
            // panel_gmap
            // 
            this.panel_gmap.BackColor = System.Drawing.Color.DarkGray;
            this.panel_gmap.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel_gmap.Location = new System.Drawing.Point(0, 0);
            this.panel_gmap.Name = "panel_gmap";
            this.panel_gmap.Size = new System.Drawing.Size(638, 644);
            this.panel_gmap.TabIndex = 0;
            // 
            // center_right
            // 
            this.center_right.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.center_right.Dock = System.Windows.Forms.DockStyle.Right;
            this.center_right.Location = new System.Drawing.Point(648, 0);
            this.center_right.Name = "center_right";
            this.center_right.Size = new System.Drawing.Size(914, 644);
            this.center_right.TabIndex = 5;
            this.center_right.Resize += new System.EventHandler(this.center_right_Resize);
            // 
            // panel2
            // 
            this.panel2.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel2.Location = new System.Drawing.Point(1562, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(0, 644);
            this.panel2.TabIndex = 4;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(38)))), ((int)(((byte)(37)))));
            this.ClientSize = new System.Drawing.Size(1582, 853);
            this.Controls.Add(this.center);
            this.Controls.Add(this.bottom);
            this.Controls.Add(this.top);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(1600, 900);
            this.Name = "MainForm";
            this.Padding = new System.Windows.Forms.Padding(10);
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Radio sync";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.top.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.appLogo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.companyLogo)).EndInit();
            this.bottom.ResumeLayout(false);
            this.panel32.ResumeLayout(false);
            this.panel_DTMF.ResumeLayout(false);
            this.panel_DTMF.PerformLayout();
            this.panel26.ResumeLayout(false);
            this.panel28.ResumeLayout(false);
            this.panel28.PerformLayout();
            this.panel27.ResumeLayout(false);
            this.panel29.ResumeLayout(false);
            this.panel31.ResumeLayout(false);
            this.panel31.PerformLayout();
            this.panel30.ResumeLayout(false);
            this.panel30.PerformLayout();
            this.panel22.ResumeLayout(false);
            this.panel23.ResumeLayout(false);
            this.panel24.ResumeLayout(false);
            this.panel24.PerformLayout();
            this.panel25.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            this.panel18.ResumeLayout(false);
            this.panel19.ResumeLayout(false);
            this.panel20.ResumeLayout(false);
            this.panel20.PerformLayout();
            this.panel21.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel14.ResumeLayout(false);
            this.panel15.ResumeLayout(false);
            this.panel16.ResumeLayout(false);
            this.panel16.PerformLayout();
            this.panel17.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.panel10.ResumeLayout(false);
            this.panel11.ResumeLayout(false);
            this.panel12.ResumeLayout(false);
            this.panel12.PerformLayout();
            this.panel13.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.panel9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.center.ResumeLayout(false);
            this.center_left.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel top;
        private System.Windows.Forms.Panel bottom;
        private System.Windows.Forms.Panel center;
        private System.Windows.Forms.PictureBox companyLogo;
        private System.Windows.Forms.PictureBox appLogo;
        private System.Windows.Forms.FlowLayoutPanel panel0;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.FlowLayoutPanel center_right;
        private System.Windows.Forms.Panel center_left;
        private System.Windows.Forms.Panel panel_gmap;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.Panel panel15;
        private System.Windows.Forms.Panel panel16;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel17;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Panel panel22;
        private System.Windows.Forms.Panel panel23;
        private System.Windows.Forms.Panel panel24;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel panel25;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.Panel panel18;
        private System.Windows.Forms.Panel panel19;
        private System.Windows.Forms.Panel panel20;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel panel21;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel panel26;
        private System.Windows.Forms.Panel panel28;
        private System.Windows.Forms.Panel panel27;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel panel29;
        private System.Windows.Forms.Panel panel30;
        private System.Windows.Forms.Panel panel31;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Panel panel32;
        private System.Windows.Forms.Panel panel_DTMF;
        private System.Windows.Forms.Label label_DTMF;
    }
}

