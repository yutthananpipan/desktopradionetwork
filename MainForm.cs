﻿using RadioNetworkDesktop.Models;
using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace RadioNetworkDesktop
{
    public partial class MainForm : Form
    {
        private MapUserControl mapUserControl;
        private PTTUserControl pttUserControl;
        private Service service;
        private AudioService audioService;

        public MainForm()
        {
            InitializeComponent();
            InitializeUserControl();
            InitializeService();
            InitializeAudioService();

        }

        private void InitializeAudioService()
        {
            audioService = new AudioService();
            audioService.MessageListening += (data) =>
            {
            };

            audioService.StartAsync();
           
        }

        private void InitializeService()
        {
            service = new Service();
            service.DisconnectedListening += () =>
            {

            };
            service.ConnectedListening += () =>
            {

            };
            service.MessageListening += (data) =>
            {

            };
            service.StartAsync();
        }

        private void InitializeUserControl()
        {
            mapUserControl = new MapUserControl();
            pttUserControl = new PTTUserControl();

            panel_gmap.Controls.Add(mapUserControl);
            center_right.Controls.Add(pttUserControl);

        }

        private void DTMF_Click(object sender, EventArgs e)
        {

        }

        private void center_right_Resize(object sender, EventArgs e)
        {
            if (this.pttUserControl != null)
            {
                this.pttUserControl.Width = this.center_right.Width;
                this.pttUserControl.Height = this.center_right.Height;
            }
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            pttUserControl.Dispose();
            mapUserControl.Dispose();
            service.Destroy();
            audioService.Distroy();
        }
    }
}
