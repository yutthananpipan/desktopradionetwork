﻿using GMap.NET;
using GMap.NET.MapProviders;
using GMap.NET.WindowsForms;
using DigitalSync.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Collections.Generic;
using DigitalSync.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Runtime.Serialization;
using GMap.NET.WindowsForms.Markers;

namespace DigitalSync
{
    public partial class MapUserControl : UserControl
    {
        private MainForm mainForm;
        public delegate void LocationChange(PointLatLng pointLatLng);
        private Dictionary<string, Device> device_marker = new Dictionary<string, Device>();

        internal readonly GMapOverlay ItemOverlay = new GMapOverlay("item");

        public MapUserControl(MainForm mainForm)
        {
            this.mainForm = mainForm;
            InitializeComponent();
            InitializeMapControl();
        }

        private void InitializeMapControl()
        {
            if (!GMapControl.IsDesignerHosted)
            {
                // set cache mode only if no internet avaible
                if (!Stuff.PingNetwork("pingtest.com"))
                {
                    map.Manager.Mode = AccessMode.CacheOnly;
                    //MessageBox.Show("No internet connection available, going to CacheOnly mode.", "GMap.NET - Demo.WindowsForms", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    map.Manager.Mode = AccessMode.ServerAndCache;
                }

                // config map         
                map.MapProvider = GMapProviders.GoogleMap;
                map.MinZoom = 12;
                map.MaxZoom = 22;
                map.Zoom = 15;
                map.Bearing = 0;

                map.Position = new PointLatLng(13.781242044308085, 100.57502300340474);
                map.DragButton = MouseButtons.Left;

                map.Overlays.Add(ItemOverlay);
            }
        }

        internal void OnMessage(RadioData data)
        {
            try
            {
                JObject obj = JsonConvert.DeserializeObject<JObject>(data.value);
                Device device = new Device(obj["id"].ToString(), obj["location"].ToString());

                // Add Data to GMap
                UpdateItem(device);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
        }

        private void UpdateItem(Device device)
        {
            try
            {
                if (device_marker.ContainsKey(device.id))
                {
                    var obj = device_marker[device.id].GmapDevice;
                    ItemOverlay.Markers.Remove(obj);
                    device_marker.Remove(device.id);
                }

                PointLatLng pointLatLng = new PointLatLng(Double.Parse(device.location.ToString().Split(", ")[0]), Double.Parse(device.location.ToString().Split(", ")[1]));
                GmapDevice gmapDevice = new GmapDevice(pointLatLng, device);

                device.GmapDevice = gmapDevice;

                // add to overlay
                device_marker.Add(device.id, device);

                ItemOverlay.Markers.Add(gmapDevice);

                this.Invoke(new MethodInvoker(delegate { map.Position = pointLatLng; map.Zoom = 18; }));
            }
            catch (Exception e)
            {
                e.ToString();
            }
        }

        private void map_Resize(object sender, EventArgs e)
        {
            Dock = DockStyle.Fill;
            map.Dock = DockStyle.Fill;
        }

        private void map_Load(object sender, EventArgs e)
        {
            this.Dock = DockStyle.Fill;
        }

        private void MapUserControl_Resize(object sender, EventArgs e)
        {
            Dock = DockStyle.Fill;
            map.Dock = DockStyle.Fill;
        }

        private void map_KeyDown(object sender, KeyEventArgs e)
        {

            if (mainForm.pttUserControl != null)
            {
                mainForm.pttUserControl.PTTButton_KeyDown(sender, e);
            }
        }

        private void map_KeyUp(object sender, KeyEventArgs e)
        {
            if (mainForm.pttUserControl != null)
            {
                mainForm.pttUserControl.PTTButton_KeyUp(sender, e);
            }

        }
    }

}
