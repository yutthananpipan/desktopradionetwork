﻿
namespace DigitalSync
{
    partial class ButtonFuncUserControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Body = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.edit = new System.Windows.Forms.PictureBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.labelName = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.labelFunctionNumber = new System.Windows.Forms.Label();
            this.labelCH = new System.Windows.Forms.Label();
            this.Body.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.edit)).BeginInit();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // Body
            // 
            this.Body.BackgroundImage = global::DigitalSync.Properties.Resources.border_function_gray;
            this.Body.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Body.Controls.Add(this.panel3);
            this.Body.Controls.Add(this.panel2);
            this.Body.Controls.Add(this.panel1);
            this.Body.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Body.Location = new System.Drawing.Point(0, 0);
            this.Body.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Body.Name = "Body";
            this.Body.Size = new System.Drawing.Size(271, 82);
            this.Body.TabIndex = 6;
            this.Body.Click += new System.EventHandler(this.button_Click);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.edit);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel3.Location = new System.Drawing.Point(233, 57);
            this.panel3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel3.Name = "panel3";
            this.panel3.Padding = new System.Windows.Forms.Padding(0, 0, 5, 5);
            this.panel3.Size = new System.Drawing.Size(38, 25);
            this.panel3.TabIndex = 3;
            this.panel3.Click += new System.EventHandler(this.edit_Click);
            // 
            // edit
            // 
            this.edit.BackgroundImage = global::DigitalSync.Properties.Resources.edit_icon;
            this.edit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.edit.Dock = System.Windows.Forms.DockStyle.Right;
            this.edit.Location = new System.Drawing.Point(10, 0);
            this.edit.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.edit.Name = "edit";
            this.edit.Size = new System.Drawing.Size(23, 20);
            this.edit.TabIndex = 3;
            this.edit.TabStop = false;
            this.edit.Click += new System.EventHandler(this.edit_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.labelName);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 27);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(271, 30);
            this.panel2.TabIndex = 1;
            this.panel2.Click += new System.EventHandler(this.button_Click);
            // 
            // labelName
            // 
            this.labelName.AutoSize = true;
            this.labelName.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelName.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.labelName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.labelName.Location = new System.Drawing.Point(0, 0);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(86, 32);
            this.labelName.TabIndex = 18;
            this.labelName.Text = "NAME";
            this.labelName.Click += new System.EventHandler(this.button_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.labelFunctionNumber);
            this.panel1.Controls.Add(this.labelCH);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(271, 27);
            this.panel1.TabIndex = 0;
            this.panel1.Click += new System.EventHandler(this.button_Click);
            // 
            // labelFunctionNumber
            // 
            this.labelFunctionNumber.AutoSize = true;
            this.labelFunctionNumber.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelFunctionNumber.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.labelFunctionNumber.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(70)))), ((int)(((byte)(70)))), ((int)(((byte)(70)))));
            this.labelFunctionNumber.Location = new System.Drawing.Point(244, 0);
            this.labelFunctionNumber.Name = "labelFunctionNumber";
            this.labelFunctionNumber.Size = new System.Drawing.Size(27, 23);
            this.labelFunctionNumber.TabIndex = 18;
            this.labelFunctionNumber.Text = "F1";
            this.labelFunctionNumber.Click += new System.EventHandler(this.button_Click);
            // 
            // labelCH
            // 
            this.labelCH.AutoSize = true;
            this.labelCH.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelCH.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.labelCH.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(70)))), ((int)(((byte)(70)))), ((int)(((byte)(70)))));
            this.labelCH.Location = new System.Drawing.Point(0, 0);
            this.labelCH.Name = "labelCH";
            this.labelCH.Size = new System.Drawing.Size(42, 23);
            this.labelCH.TabIndex = 17;
            this.labelCH.Text = "CH1";
            this.labelCH.Click += new System.EventHandler(this.button_Click);
            // 
            // ButtonFuncUserControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.Body);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "ButtonFuncUserControl";
            this.Size = new System.Drawing.Size(271, 82);
            this.Resize += new System.EventHandler(this.ButtonFuncUserControl_Resize);
            this.Body.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.edit)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel Body;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label labelCH;
        private System.Windows.Forms.Label labelFunctionNumber;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label labelName;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.PictureBox edit;
    }
}
