﻿using DigitalSync.Models;
using DigitalSync.Utils;
using NAudio.CoreAudioApi;
using NAudio.Wave;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DigitalSync
{
    public partial class PTTUserControl : UserControl
    {
        private int func_record = 1;
        private bool isDataIncomming;
        private bool isSocketConnected;
        private bool isPTT = false;
        private List<RecordUserControl> recordPanels = new List<RecordUserControl>();
        private List<Profile> profiles;

        // Audio Recording
        int Counter = 0;
        WaveIn _waveIn;
        int sampleRate = 44100;
        byte[] _notEncodedBuffer = new byte[0];

        // Volume
        private MMDevice device;

        private UdpClient udpClient;

        Thread thread_connect;
        Thread thread_listening;
        private static int AUDIO_PORT = 65432;

        private MainForm mainForm;
        private int pttCounter;

        public PTTUserControl(MainForm mainForm)
        {
            this.mainForm = mainForm;
            InitializeComponent();
            InitData();
            InitListButtonChannelControl();

            StartAsync();

            var deviceEnumerator = new MMDeviceEnumerator();
            device = deviceEnumerator.GetDefaultAudioEndpoint(DataFlow.Render, Role.Multimedia);

        }

        private void PTTUserControl_Load(object sender, EventArgs e)
        {
            device.AudioEndpointVolume.OnVolumeNotification += AudioEndpointVolume_OnVolumeNotification;
            this.KeyDown += new KeyEventHandler(this.PTTButton_KeyDown);
            this.KeyUp += new KeyEventHandler(this.PTTButton_KeyUp);

            UpdateVolume();
            UpdateMuted();
        }

        void AudioEndpointVolume_OnVolumeNotification(AudioVolumeNotificationData data)
        {
            try
            {
                this.Invoke(new Action(delegate ()
                {
                    UpdateVolume();
                    UpdateMuted();
                }));
            }
            catch
            {
            }
        }

        public void UpdateVolume()
        {
            try
            {
                Image[] level_imgs = {
                    null, Properties.Resources.volume_level_1, Properties.Resources.volume_level_2,
                    Properties.Resources.volume_level_3, Properties.Resources.volume_level_4, Properties.Resources.volume_level_5,
                    Properties.Resources.volume_level_6, Properties.Resources.volume_level_7, Properties.Resources.volume_level_8,
                    Properties.Resources.volume_level_9, Properties.Resources.volume_level_10
                };

                int volume = (int)(Math.Round(device.AudioEndpointVolume.MasterVolumeLevelScalar * 100));
                int level = (volume > 0 && volume < 10) ? 1 : volume / 10;

                this.volumeLevel.BackgroundImage = level_imgs[level];
                this.Speaker.BackgroundImage = (level >= 7) ? Properties.Resources.speaker_full : Properties.Resources.speaker_medium;
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
        }

        public void UpdateMuted()
        {
            try
            {
                bool mute;
                mute = device.AudioEndpointVolume.Mute;
                if (mute)
                {
                    this.Speaker.BackgroundImage = Properties.Resources.speaker_mute;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
        }

        private void InitListButtonChannelControl()
        {
            profiles = Profile.GetProfiles();
            for (int ri = 0; ri < profiles.Count() / 3; ri++)
            {
                List<ButtonFuncUserControl> buttons = new List<ButtonFuncUserControl>();
                for (int bi = 0; bi < 3; bi++)
                {
                    int pi = ((bi + 1) + (ri * 3)) - 1;
                    ButtonFuncUserControl buttonFuncUser = new ButtonFuncUserControl(this, profiles[pi]);
                    buttonFuncUser.CH.Text = "CH" + profiles[pi].Channel;
                    buttonFuncUser.FUNCTION.Text = "F" + ((bi + 1) + (ri * 3)).ToString();
                    buttonFuncUser.NAME.Text = profiles[pi].Name;
                    buttonFuncUser.InitTextDisplay();

                    buttonFuncUser.selectListening += () =>
                    {
                        int pSelect = Properties.Settings.Default.profile_select;
                        Profile p = profiles[pSelect];
                        Properties.Settings.Default.CHANNELID = p.Channel;
                        Properties.Settings.Default.NAME = p.Name;
                        Properties.Settings.Default.Save();

                        InitData();
                        mainForm.Service.SocketSendData(
                           new RadioData(NameType.CHANNEL.ToString(), ActionType.set.ToString(), "CH" + p.Channel)
                        );

                    };

                    buttons.Add(buttonFuncUser);
                }

                recordPanels.Add(new RecordUserControl(buttons));
            }
        }

        internal void OnConnected()
        {
            try
            {
                isSocketConnected = true;
                this.labelPTTStatus_Display.Invoke(new MethodInvoker(delegate { this.labelPTTStatus_Display.Text = "PTT Standby"; }));
                this.label_SocketStatus.Invoke(new MethodInvoker(delegate { label_SocketStatus.Text = "Connected"; }));

                mainForm.Service.SocketSendData(
                     new RadioData(NameType.CHANNEL.ToString(), ActionType.get.ToString(), "")
                );

                mainForm.Service.SocketSendData(
                     new RadioData(NameType.UID.ToString(), ActionType.get.ToString(), "")
                );
            }
            catch (Exception e)
            {
                e.ToString();
            }

        }

        internal void OnDisonnect()
        {
            try
            {
                isSocketConnected = false;
                this.labelPTTStatus_Display.Invoke(new MethodInvoker(delegate { this.labelPTTStatus_Display.Text = "Disconnected"; }));
                this.label_SocketStatus.Invoke(new MethodInvoker(delegate { label_SocketStatus.Text = "Disconnected"; }));
            }
            catch (Exception e)
            {
                e.ToString();
            }
        }

        internal void _timer_Tick(object sender, EventArgs e)
        {
            if (!isDataIncomming && !isPTT)
            {
                Counter = 0;
                string formatTime = "HH:mm tt";
                DateTime now = DateTime.Now;

                this.Invoke(new MethodInvoker(delegate
                {
                    this.labelTime_Display.Text = now.ToString(formatTime);
                    this.labelPTTStatus_Display.ForeColor = Color.Gray;
                    this.labelPTTStatus_Display.Text = (this.isSocketConnected) ? "PTT Standby" : "Disconnected";
                }));

                PTTStyleChange(NameType.TERMINATED);

            }
            else
            {
                Counter++;
                this.Invoke(new MethodInvoker(delegate
                {
                    this.labelTime_Display.Text = ((Counter / 60) >= 10 ? (Counter / 60).ToString() : "0" + (Counter / 60)) + ":" + ((Counter % 60) >= 10 ? (Counter % 60).ToString() : "0" + (Counter % 60));
                    this.labelPTTStatus_Display.Text = (isDataIncomming) ? "Receiving" : "Transmitting";
                    this.labelPTTStatus_Display.ForeColor = (isDataIncomming) ? Color.Red : Color.Green;
                }));
            }

            isDataIncomming = false;
            InitTextDisplay();

        }

        internal void OnAudioIn(RadioData data)
        {
            PTTStyleChange(NameType.INCOMING);
        }

        internal void OnMessage(RadioData data)
        {
            try
            {
                if (data.name == NameType.CHANNEL.ToString() && data.action == ActionType.set.ToString())
                {
                    int value = Int32.Parse(data.value.Split("CH")[1]);
                    Properties.Settings.Default.CHANNELID = value;
                }
                else if (data.name == NameType.UID.ToString() && data.action == ActionType.set.ToString())
                {
                    string value = data.value.Split("UID")[1];
                    Properties.Settings.Default.UID = value;
                }
                else if (data.name == NameType.INCOMING.ToString() && data.action == ActionType.set.ToString())
                {
                    // Save History
                    string value = (data.value == "") ? "Unknown" : data.value;
                    HistoryCall h = new HistoryCall();
                    h.Zone = Properties.Settings.Default.ZONEID;
                    h.Channel = Properties.Settings.Default.CHANNELID;
                    h.Uid = value;
                    h.Datetime = DateTime.Now;

                    HistoryCall.Insert(h);
                }

                Properties.Settings.Default.Save();

                this.Invoke(new MethodInvoker(delegate { InitData(); }));

            }
            catch (Exception e)
            {
                e.ToString();
            }
        }

        private void InitData()
        {
            this.labelUUID.Text = Properties.Settings.Default.UID;
            this.labelCH_Display.Text = "CH" + Properties.Settings.Default.CHANNELID;
            this.labelSendUID_Display.Text = Properties.Settings.Default.SENDID;
            this.labelName_Display.Text = Properties.Settings.Default.NAME.Trim();

            InitTextDisplay();

            foreach (var rp in recordPanels)
            {
                foreach (var buttonControl in rp.buttonFuncUserControls)
                {
                    buttonControl.InitTextDisplay();
                }
            }

        }

        private void InitTextDisplay()
        {
            this.labelSendUID_Display.Parent.Padding = new Padding((labelSendUID_Display.Parent.Width - labelSendUID_Display.Width) / 2, 0, 0, 0);
            this.labelName_Display.Parent.Padding = new Padding((labelName_Display.Parent.Width - labelName_Display.Width) / 2, 0, 0, 0);
            this.labelTime_Display.Parent.Padding = new Padding((labelTime_Display.Parent.Width - labelTime_Display.Width) / 2, 0, 0, 0);
            this.labelPTTStatus_Display.Parent.Padding = new Padding((labelPTTStatus_Display.Parent.Width - labelPTTStatus_Display.Width) / 2, 0, 0, 0);
        }

        private void ChangeRecordFunction()
        {
            this.panelFunction.Controls.Clear();
            for (int ri = func_record - 1; ri >= 0; ri--)
            {
                this.panelFunction.Invoke(new MethodInvoker(delegate { this.panelFunction.Controls.Add(recordPanels[ri]); }));
            }
        }

        private void PTTUserControl_SizeChanged(object sender, EventArgs e)
        {
            if (recordPanels.Count() > 0)
            {
                int h_Func = this.panelFunction.Height;
                int h_Record = this.recordPanels[0].Height;

                int record = h_Func / h_Record;
                if (record != func_record)
                {
                    func_record = record;

                    // new object here
                    ChangeRecordFunction();
                    Console.WriteLine("PTT Size change record : " + func_record.ToString());
                }
            }
        }

        public void StartAsync()
        {
            thread_connect = new Thread(ClientConnect);
            thread_listening = new Thread(ClientListening);

            thread_connect.IsBackground = true;
            thread_listening.IsBackground = true;
            thread_connect.Start();
            thread_listening.Start();

        }

        public void OnDestroy()
        {
            try
            {
                OnDisonnect();
                thread_connect.Abort();
                thread_listening.Abort();
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
        }

        private void ClientConnect()
        {
            try
            {
                string ADDRESS = Properties.Settings.Default.SocketURL;

                udpClient = new UdpClient();
                udpClient.Connect(ADDRESS, Service.AUDIO_PORT);

            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        private void ClientListening()
        {
            try
            {
                UdpClient udpServer = new UdpClient(Service.AUDIO_PORT);

                WaveOut _waveOut = new WaveOut(WaveCallbackInfo.FunctionCallback());
                BufferedWaveProvider _playBuffer;
                _playBuffer = new BufferedWaveProvider(new WaveFormat(sampleRate, 16, 1));
                _waveOut.Init(_playBuffer);
                _waveOut.Play();

                while (true)
                {
                    var remoteEP = new IPEndPoint(IPAddress.Any, AUDIO_PORT);
                    byte[] buff = udpServer.Receive(ref remoteEP); // listen on port 11000

                    try
                    {
                        isDataIncomming = true;
                        _playBuffer.AddSamples(buff, 0, buff.Length);

                        this.Invoke((MethodInvoker)delegate
                        {
                            PTTStyleChange(NameType.INCOMING);

                        });
                    }
                    catch (Exception e)
                    {
                        e.ToString();
                    }

                }
            }
            catch (Exception e)
            {
                e.ToString();
            }
        }

        private void PTTStyleChange(NameType nameType)
        {
            if (nameType == NameType.EndCall || nameType == NameType.TERMINATED)
            {
                // Default
                #region
                this.PTTButton.AlternativeFocusBorderColor = Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(173)))), ((int)(((byte)(240)))));
                this.PTTButton.BackColor = Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(173)))), ((int)(((byte)(240)))));
                this.PTTButton.ForeColor = Color.Lime;
                this.PTTButton.InnerBorderColor = Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(178)))), ((int)(((byte)(248)))));
                this.PTTButton.OuterBorderColor = Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(178)))), ((int)(((byte)(248)))));
                this.PTTButton.ShineColor = Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(178)))), ((int)(((byte)(248)))));
                #endregion
            }
            else if (nameType == NameType.StartCall)
            {
                // PTT
                #region
                this.PTTButton.AlternativeFocusBorderColor = Color.SeaGreen;
                this.PTTButton.BackColor = Color.Green;
                this.PTTButton.ForeColor = Color.White;
                this.PTTButton.InnerBorderColor = Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
                this.PTTButton.OuterBorderColor = Color.Green;
                this.PTTButton.ShineColor = Color.Lime;
                #endregion
            }
            else if (nameType == NameType.INCOMING)
            {
                // Incoming
                #region
                this.PTTButton.AlternativeFocusBorderColor = Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(173)))), ((int)(((byte)(240)))));
                this.PTTButton.BackColor = Color.Red;
                this.PTTButton.ForeColor = Color.White;
                this.PTTButton.InnerBorderColor = Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
                this.PTTButton.OuterBorderColor = Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
                this.PTTButton.ShineColor = Color.Red;
                #endregion
            }
        }

        void StartRecording()
        {
            isPTT = true;
            _waveIn = new WaveIn(WaveCallbackInfo.FunctionCallback());
            _waveIn.BufferMilliseconds = 10;
            _waveIn.DataAvailable += _waveIn_DataAvailable;
            _waveIn.WaveFormat = new WaveFormat(sampleRate, 16, 1);

            _waveIn.StartRecording();

        }

        void StopRecording()
        {
            isPTT = false;
            _waveIn.StopRecording();
            _waveIn.Dispose();
            _waveIn = null;

        }

        void _waveIn_DataAvailable(object sender, WaveInEventArgs e)
        {
            byte[] soundBuffer = new byte[e.BytesRecorded + _notEncodedBuffer.Length];

            for (int i = 0; i < _notEncodedBuffer.Length; i++)
                soundBuffer[i] = _notEncodedBuffer[i];
            for (int i = 0; i < e.BytesRecorded; i++)
                soundBuffer[i + _notEncodedBuffer.Length] = e.Buffer[i];
            try
            {
                udpClient.Send(e.Buffer, e.Buffer.Length);
            }
            catch (Exception send_exception)
            {
                Console.WriteLine(" Exception {0}", send_exception.Message);
            }
        }

        public void PTTDown()
        {
            try
            {
                PTTStyleChange(NameType.StartCall);
                StartRecording();
                mainForm.Service.SocketSendData(new RadioData(NameType.StartCall.ToString(), ActionType.set.ToString(), labelSendUID_Display.Text.ToString()));

                pttCounter++;
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
        }

        public void PTTUp()
        {
            try
            {
                PTTStyleChange(NameType.EndCall);
                StopRecording();
                mainForm.Service.SocketSendData(
                    new RadioData(NameType.EndCall.ToString(), ActionType.set.ToString(), labelSendUID_Display.Text.ToString())
                );

                pttCounter = 0;
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
        }

        public void PTTButton_KeyDown(object sender, KeyEventArgs e)
        {
            if (pttCounter == 0 && e.KeyCode == Keys.Space)
            {
                PTTDown();
            }
        }

        private void PTTButton_MouseDown(object sender, MouseEventArgs e)
        {
            if (pttCounter == 0)
            {
                PTTDown();
            }
        }

        public void PTTButton_KeyUp(object sender, KeyEventArgs e)
        {
            if (pttCounter > 0)
            {
                PTTUp();
            }
        }

        private void PTTButton_MouseUp(object sender, MouseEventArgs e)
        {
            if (pttCounter > 0)
            {
                PTTUp();
            }
        }

        private void channelUp_Click(object sender, EventArgs e)
        {
            try
            {
                int ch = Properties.Settings.Default.CHANNELID;
                ch++;
                Properties.Settings.Default.CHANNELID = ch;
                Properties.Settings.Default.profile_select = profiles.Find(obj => obj.Channel == ch).Id;
                Properties.Settings.Default.Save();

                InitData();
                mainForm.Service.SocketSendData(
                   new RadioData(NameType.CHANNEL.ToString(), ActionType.set.ToString(), "CH" + Properties.Settings.Default.CHANNELID)
                );
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
        }

        private void channelDown_Click(object sender, EventArgs e)
        {
            try
            {
                int ch = Properties.Settings.Default.CHANNELID;
                if (ch > 1)
                {
                    ch--;
                    Properties.Settings.Default.CHANNELID = ch;
                    Properties.Settings.Default.profile_select = profiles.Find(obj => obj.Channel == ch).Id;
                    Properties.Settings.Default.Save();

                    InitData();
                    mainForm.Service.SocketSendData(
                         new RadioData(NameType.CHANNEL.ToString(), ActionType.set.ToString(), "CH" + Properties.Settings.Default.CHANNELID)
                    );
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
        }

        private void Speaker_Click(object sender, EventArgs e)
        {
            this.KeyDown += new KeyEventHandler(this.PTTButton_KeyDown);
            this.KeyUp += new KeyEventHandler(this.PTTButton_KeyUp);
        }

        private void PTTButton_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void PTTUserControl_Click(object sender, EventArgs e)
        {
            this.KeyDown += new KeyEventHandler(this.PTTButton_KeyDown);
            this.KeyUp += new KeyEventHandler(this.PTTButton_KeyUp);
        }

        private void EmergencyButton_Click(object sender, EventArgs e)
        {
            mainForm.Service.SocketSendData(new RadioData(NameType.EMERGENCY.ToString(), ActionType.set.ToString(), labelUUID.Text.ToString()));

        }
    }

    public class MuteEventArgs : EventArgs
    {
        public MuteEventArgs(bool muted)
        {
            Muted = muted;
        }

        public bool Muted { get; private set; }
    }

    public class VolumeEventArgs : EventArgs
    {
        public VolumeEventArgs(float volume)
        {
            Volume = volume;
        }

        public float Volume { get; private set; }
    }
}
