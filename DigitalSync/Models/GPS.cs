﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigitalSync.Models
{
    class GPS
    {
        private string NAME;
        private string ACTION;
        private List<Device> VALUE;

        public GPS(string nAME, string aCTION, List<Device> vALUE)
        {
            NAME = nAME;
            ACTION = aCTION;
            VALUE = vALUE;
        }

        public string name { get => NAME; set => NAME = value; }
        public string action { get => ACTION; set => ACTION = value; }
        public List<Device> value { get => VALUE; set => VALUE = value; }

    }
}
