﻿using DigitalSync.Models;
using DigitalSync.Utils;
using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace DigitalSync
{
    public partial class MainForm : Form
    {
        public MapUserControl mapUserControl;
        public PTTUserControl pttUserControl;
        private Service service;

        Timer _timer = null;

        internal Service Service { get => service; }

        public MainForm()
        {
            InitializeComponent();
            InitializeUserControl();
            InitializeService();
            InitTimer();

        }

        private void InitTimer()
        {
            if (_timer == null)
            {
                _timer = new Timer();
                _timer.Interval = 1000;
                _timer.Tick += _timer_Tick;
            }
            _timer.Start();
        }

        private void InitializeService()
        {
            service = new Service();
            try
            {
                service.DisconnectedListening += () =>
                {
                    pttUserControl.OnDisonnect();
                };
                service.ConnectedListening += () =>
                {
                    pttUserControl.OnConnected();
                };
                service.MessageListening += (data) =>
                {
                    if(data.name == NameType.GPS.ToString())
                    {
                        mapUserControl.OnMessage(data);
                    }
                    else
                    {
                        pttUserControl.OnMessage(data);
                    }
                };

            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            service.StartAsync();
        }

        private void InitializeUserControl()
        {
            mapUserControl = new MapUserControl(this);
            pttUserControl = new PTTUserControl(this);

            panel_gmap.Controls.Add(mapUserControl);
            center_right.Controls.Add(pttUserControl);

        }

        void _timer_Tick(object sender, EventArgs e)
        {
            UpdateDigitalClock();
            if (pttUserControl != null)
            {
                pttUserControl._timer_Tick(sender, e);
            }
            Console.WriteLine("{0} Bps", "");
        }

        private void DTMF_Click(object sender, EventArgs e)
        {

        }

        private void UpdateDigitalClock()
        {
            string formatTime = "HH:mm:ss tt";
            string formatDate = "dd MMMM yyyy";
            DateTime now = DateTime.Now;
            this.labelTime.Text = now.ToString(formatTime);
            this.labelDate.Text = now.ToString(formatDate);
        }

        private void center_right_Resize(object sender, EventArgs e)
        {
            if (pttUserControl != null)
            {
                pttUserControl.Width = this.center_right.Width;
                pttUserControl.Height = this.center_right.Height;

                pttUserControl.KeyDown += new KeyEventHandler(pttUserControl.PTTButton_KeyDown);
                pttUserControl.KeyUp += new KeyEventHandler(pttUserControl.PTTButton_KeyUp);
            }
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            pttUserControl.OnDestroy();
            pttUserControl.Dispose();
            mapUserControl.Dispose();
            service.Destroy();
            _timer.Stop();
        }

        private void Settings_Click(object sender, EventArgs e)
        {
            pttUserControl.KeyDown += new KeyEventHandler(pttUserControl.PTTButton_KeyDown);
            pttUserControl.KeyUp += new KeyEventHandler(pttUserControl.PTTButton_KeyUp);

            DialogResult result = Settings.MSGShow();
            try
            {
                if (result == DialogResult.OK)
                {
                    service.StartAsync();

                    pttUserControl.OnDestroy();
                    pttUserControl.StartAsync();

                }else if (result == DialogResult.No)
                {
                    service.Destroy();
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
        }

        private void location_Click(object sender, EventArgs e)
        {
            DialogResult result = LocationDialog.MSGShow(this);

            pttUserControl.KeyDown += new KeyEventHandler(pttUserControl.PTTButton_KeyDown);
            pttUserControl.KeyUp += new KeyEventHandler(pttUserControl.PTTButton_KeyUp);
        }

        private void History_Click(object sender, EventArgs e)
        {
            HistoryForm.MSGShow();
        }
    }
}
