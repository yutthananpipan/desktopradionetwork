﻿using DigitalSync.Models;
using SQLite;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Text;
using System.Windows.Forms;
using static System.Net.Mime.MediaTypeNames;
using Application = System.Windows.Forms.Application;

namespace DigitalSync.DB
{
    class DBProvider
    {
        public static SQLiteConnection CreateConnection()
        {
            string appPath = Path.GetFullPath(Application.ExecutablePath);
            var databasePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "DIGITALSYNC.db");
            var db = new SQLiteConnection("DIGITALSYNC.db");
            db.CreateTable<Profile>();
            db.CreateTable<HistoryCall>();

            return db;

        }
    }
}
