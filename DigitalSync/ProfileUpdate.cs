﻿using DigitalSync.DB;
using DigitalSync.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace DigitalSync
{
    public partial class ProfileUpdate : Form
    {
        static ProfileUpdate MsgBox;
        static DialogResult result = DialogResult.No;
        private ButtonFuncUserControl buttonFuncUser;

        public ProfileUpdate(ButtonFuncUserControl buttonFuncUser)
        {
            this.buttonFuncUser = buttonFuncUser;
            InitializeComponent();
            InitialData();
        }

        private void InitialData()
        {
            textID.Text = buttonFuncUser.profile.Id.ToString();
            textChannel.Text = buttonFuncUser.profile.Channel.ToString();
            textName.Text = buttonFuncUser.profile.Name;
            textRemark.Text = buttonFuncUser.profile.Remark;

        }

        public static DialogResult MSGShow(ButtonFuncUserControl buttonFuncUser)
        {
            MsgBox = new ProfileUpdate(buttonFuncUser);
            MsgBox.ShowDialog();
            return result;
        }

        private void CloseButton_Click(object sender, EventArgs e)
        {
            result = DialogResult.No;
            MsgBox.Close();
        }

        private void UpdateButton_Click(object sender, EventArgs e)
        {
            result = DialogResult.OK;

            try
            {
                buttonFuncUser.profile.Channel = Int32.Parse(textChannel.Text);
                buttonFuncUser.profile.Name = textName.Text.ToString();
                buttonFuncUser.profile.Remark = textRemark.Text.ToString();

                Profile.Update(buttonFuncUser.profile);
            }
            catch (Exception ex)
            {
                ex.ToString();
                MessageBox.Show("Can not update data, Please check input again.", "Warning!!!", MessageBoxButtons.OK);
            }

            MsgBox.Close();
        }
    }
}
