﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigitalSync.Utils
{
    public enum NameType
    {
        GPS,
        StartCall,
        EndCall,
        ZONE,
        UID,
        CHANNEL,
        INCOMING,
        TERMINATED,
        EMERGENCY
    }
}
